//
//  ImageGridCollectionViewController.swift
//  secondapp
//
//  Created by Ivan Bella López on 11/07/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

import UIKit

extension UIImage {
    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
        var rect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        var image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

let reuseIdentifier = "Cell"

class ImageGridCollectionViewController: UICollectionViewController {
    
    var idsListArray: Array<String>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parseIdsList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // SDWebImage clears automatically the cache in case a memory warning is received
    }
    
    
    // MARK: - UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return idsListArray.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ImageCollectionViewCell
        
        configureCell(cell, indexPath: indexPath)
        
        return cell
    }
    
    
    // MARK: - Private
    
    func parseIdsList() {
        let path = NSBundle.mainBundle().pathForResource("ids_list", ofType: "txt")
        
        if let content = String(contentsOfFile:path!, encoding: NSUTF8StringEncoding, error: nil) {
            idsListArray = content.componentsSeparatedByString("\n")
        }
    }
    
    func configureCell(cell: ImageCollectionViewCell, indexPath: NSIndexPath) {
        
        // 1. Create URL with the id according to the index path
        let idString = NSString(format:"%@", idsListArray[indexPath.row]) as String
        
        let url = NSURL(string: "https://www.nicequest.com/portal_nicequest_api/DocumentServlet?docid="
            + idString)
        
        // 2. Show the id of the picture for testing purposes
        cell.label.text = idString
        
        // 3. Create a red placeholder in case no image is available
        let placeholderImage = UIImage().getImageWithColor(UIColor.redColor(), size: cell.frame.size)
        
        // 4. Load the image using SDWebImage
        // SDWebImage loads images asynchronously and manage the cache so we don't worry about it
        cell.imageView.alpha = 0.0
        cell.imageView.sd_setImageWithURL(url, placeholderImage: placeholderImage, options: SDWebImageOptions.DelayPlaceholder) { (image, error, cacheType, url) -> Void in
            
            // Only animate once per image
            if cacheType == SDImageCacheType.None {
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    cell.imageView.alpha = 1.0
                    }, completion: nil)
            } else {
                cell.imageView.alpha = 1.0
            }
        }
    }
}
