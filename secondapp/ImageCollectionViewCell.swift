//
//  ImageCollectionViewCell.swift
//  secondapp
//
//  Created by Ivan Bella López on 11/07/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!

    override func prepareForReuse() {
        imageView.image = nil
        label.text = nil
    }
}

