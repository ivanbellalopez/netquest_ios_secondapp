//
//  CollectionViewCell.h
//  secondapp
//
//  Created by Ivan Bella López on 11/07/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@end
